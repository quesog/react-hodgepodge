# React Hodgepodge Presentation

## Topics

- Typescript
- lazy and Suspense
- Hooks
  - useState
  - useEffect
  - Custom hooks

## Useful Links

- [Official Hooks Documentation](https://reactjs.org/docs/hooks-intro.html)
- [Short videos on using Hooks](https://egghead.io/playlists/react-hooks-and-suspense-650307f2)
- [Daily Hook Examples](https://usehooks.com/)
