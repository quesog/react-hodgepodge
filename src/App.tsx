import React from "react";
import { NavLink, Switch, Route, BrowserRouter } from "react-router-dom";

import HooksDemo from "./hooks/HooksDemo";
import TypesDemo from "./types/TypesDemo";
import SuspenseDemo from "./suspense/SuspenseDemo";
import Home from "./Home";

import "./App.css";
import EffectsDemo from "./effects/EffectsDemo";
import CustomHookDemo from "./customhook/CustomHookDemo";

class App extends React.Component {
  public render() {
    return (
      <BrowserRouter>
        <div>
          <ul className="AppNav">
            <li>
              <NavLink to="/" exact activeClassName="active">
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to="/types">TypeScript</NavLink>
            </li>
            <li>
              <NavLink to="/suspense">Suspense</NavLink>
            </li>
            <li>
              <NavLink to="/hooks">Hooks</NavLink>
            </li>
            <li>
              <NavLink to="/effects">Effects</NavLink>
            </li>
            <li>
              <NavLink to="/customhook">Custom Hooks</NavLink>
            </li>
          </ul>
          <div>
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/types" component={TypesDemo} />
              <Route path="/suspense" component={SuspenseDemo} />
              <Route path="/hooks" component={HooksDemo} />
              <Route path="/effects" component={EffectsDemo} />
              <Route path="/customhook" component={CustomHookDemo} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
