import * as React from "react";

export default function SlowLoadingComponent() {
  return <div>This section of the application was loaded asynchronously.</div>;
}
