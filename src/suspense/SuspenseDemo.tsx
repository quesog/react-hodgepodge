import React, { Suspense, lazy } from "react";

const SlowLoadingComponent = lazy(() => import("./SlowLoadingComponent"));

export default function SuspenseDemo() {
  return (
    <div>
      <h2>Suspense Demo</h2>

      <Suspense fallback={<div>Loading...</div>}>
        <SlowLoadingComponent />
      </Suspense>
    </div>
  );
}
