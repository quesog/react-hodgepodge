import React from "react";

export default class BeforeState extends React.Component {
  public state = {
    count: 0,
    name: ""
  };

  public increment = () => {
    this.setState({ count: this.state.count + 1 });
  };

  public render() {
    return (
      <div>
        The count is {this.state.count}.
        <br />
        <button onClick={this.increment}>Increment</button>
        <br />
        Enter your name:{" "}
        <input
          type="text"
          value={this.state.name}
          onChange={event => this.setState({ name: event.target.value })}
        />
        <br />
        <p>Your name is {this.state.name}.</p>
      </div>
    );
  }
}
