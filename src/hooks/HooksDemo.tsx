import * as React from "react";
import BeforeState from "./BeforeState";
import AfterState from "./AfterState";

export default function HooksDemo() {
  return (
    <div>
      <h2>Hooks Demo</h2>

      <div>The following component is written with pre-hook state.</div>
      <BeforeState />

      <hr />

      <div>The following component is written with hook-based state.</div>
      <AfterState />
    </div>
  );
}
