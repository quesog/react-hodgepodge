import React, { useState } from "react";

export default function AfterState() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  return (
    <div>
      The count is: {count}
      <br />
      <button onClick={() => setCount(count + 1)}>Increment</button>
      <br />
      Enter your name:{" "}
      <input
        type="text"
        value={name}
        onChange={event => setName(event.target.value)}
      />
      <br />
      <p>Your name is {name}</p>
    </div>
  );
}
