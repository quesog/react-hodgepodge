import React, { useState, useEffect } from "react";

export default function EffectsDemo() {
  const initialCount = Number(window.localStorage.getItem("count")) || 0;
  const [count, setCount] = useState(initialCount);

  useEffect(
    () => {
      document.title = `Click Count: ${count}`;

      return () => {
        document.title = "";
      };
    },
    [count]
  );

  useEffect(() => {
    window.localStorage.setItem("count", count.toString());
  });

  return (
    <div>
      <h2>Effects Demo</h2>
      <button onClick={() => setCount(count + 1)}>Click Count: {count}</button>
    </div>
  );
}
