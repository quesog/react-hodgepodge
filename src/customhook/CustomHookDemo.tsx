import React, { useEffect, useState } from "react";

function usePersistentState(initialValue: any, valueName: string) {
  initialValue = window.localStorage.getItem(valueName) || initialValue;
  const [value, setValue] = useState(initialValue);
  useEffect(
    () => {
      window.localStorage.setItem(valueName, value);
    },
    [value]
  );

  return [value, setValue];
}

export default function CustomHookDemo() {
  let [clickCount, setClickCount] = usePersistentState(0, "clickCount");
  clickCount = Number(clickCount);
  return (
    <div>
      <h2>Custom Hook Demo</h2>
      <button onClick={() => setClickCount(clickCount + 1)}>
        Click Count: {clickCount}
      </button>
    </div>
  );
}
