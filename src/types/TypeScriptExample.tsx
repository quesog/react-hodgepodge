import * as React from "react";

interface ExampleProps {
  name: string;
  number: number;
}

function TypeScriptExample(props: ExampleProps) {
  return (
    <div>
      Hello, {props.name}. You passed me the number {props.number}.
    </div>
  );
}

export default TypeScriptExample;
