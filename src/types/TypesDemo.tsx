import * as React from "react";
import PropTypesExample from "./PropTypesExample";
import TypeScriptExample from "./TypeScriptExample";

export default function TypesDemo() {
  return (
    <div>
      <h2>Types Demo</h2>

      <div>
        The following component is written in JavaScript with PropTypes.
      </div>
      <PropTypesExample name="Chad" number="32" />

      <hr />

      <div>The following component is written in TypeScript.</div>
      <TypeScriptExample name="Chad" number={32} />
    </div>
  );
}
