import React from "react";
import PropTypes from "prop-types";

function PropTypesExample(props) {
  return (
    <div>
      Hello, {props.name}. You passed me the number {props.number}.
    </div>
  );
}

PropTypesExample.propTypes = {
  name: PropTypes.string.isRequired,
  number: PropTypes.number.isRequired
};

export default PropTypesExample;
