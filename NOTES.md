---
title: Notes
---

1. Typescript

  - `create-react-app` v2.1 comes with out-of-the-box Typescript support.
  - DEMO
    - Example with prop-types package, errors in typing are logged to the console at runtime.
      - Remove prop
      - Change prop to string
    - Example with TypeScript, errors in typing appear at compile-time.

2. React.Suspense and React.lazy

- Used to do code splitting
- Dial down the network speed in devtools
- Show network traffic - separately loaded JS file
- Note the dynamic import function

3. Hooks

- New way to do state and other stuff in functional components - no class required
- Currently a feature proposal in Alpha
- Live code a state example in the old way:

```javascript
import React from "react";

export default class BeforeState extends React.Component {
  public state = {
    count: 0,
    name: ""
  };

  public increment = () => {
    this.setState({ count: this.state.count + 1 });
  };

  public render() {
    return (
      <div>
        The count is {this.state.count}.
        <br />
        <button onClick={this.increment}>Increment</button>
        <br />
        Enter your name:{" "}
        <input
          type="text"
          value={this.state.name}
          onChange={event => this.setState({ name: event.target.value })}
        />
        <br />
        <p>Your name is {this.state.name}.</p>
      </div>
    );
  }
}
```

- Live code an example the new way

```javascript
import React, { useState } from "react";

export default function AfterState() {
  const [count, setCount] = useState(0);
  const [name, setName] = useState("");

  return (
    <div>
      The count is: {count}
      <br />
      <button onClick={() => setCount(count + 1)}>Increment</button>
      <br />
      Enter your name:{" "}
      <input
        type="text"
        value={name}
        onChange={event => setName(event.target.value)}
      />
      <br />
      <p>Your name is {name}</p>
    </div>
  );
}
```

- The name doesn't matter. React internally keeps track by the order in which `useState` called
- Don't put `useState` in a conditional.

4. Effects

- `useEffect` is another hook for "side effects" (data fetching, DOM manipulation, etc)
- Basically `componentDidMount`, `componentDidUpdate`, and `componentWillUnmount` combined
- Live coding demo:

```javascript
import React, { useState, useEffect } from "react";

export default function EffectsDemo() {
  const initialCount = Number(window.localStorage.getItem("count")) || 0;
  const [count, setCount] = useState(initialCount);

  useEffect(() => {
    // "componentDidMount/Update"
    document.title = `You clicked ${count} times`;

    // "componentWillUnmount"
    return () => {
      document.title = "";
    };
  });

  useEffect(
    () => {
      window.localStorage.setItem("count", count.toString());
    },
    [count]
  );

  return (
    <div>
      <h2>Effects Demo</h2>
      <button onClick={() => setCount(count + 1)}>Click Count: {count}</button>
    </div>
  );
}
```

- There are other hooks available
  - `useReducer` - Advanced `useState`, works like Redux
  - `useContext` - Access to React context values (global state)
  - `useRef` - Mutable reference that persists for the full component lifetime

5. Custom Hook

- You can write your own hooks - this one encapsulates the persistance from the last example

```javascript
import React, { useEffect, useState } from "react";

function usePersistentValue(initialValue: any, valueName: string) {
  initialValue = window.localStorage.getItem(valueName) || initialValue;
  const [value, setValue] = useState(initialValue);
  useEffect(
    () => {
      window.localStorage.setItem(valueName, value);
    },
    [value]
  );

  return [value, setValue];
}

export default function CustomHookDemo() {
  let [clickCount, setClickCount] = usePersistentValue(0, "clickCount");
  clickCount = Number(clickCount);
  return (
    <div>
      <h2>Custom Hook Demo</h2>
      <button onClick={() => setClickCount(clickCount + 1)}>
        Click Count: {clickCount}
      </button>
    </div>
  );
}
```
